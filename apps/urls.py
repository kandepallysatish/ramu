from django.contrib import admin
from django.urls import path, include
from apps import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path("", views.index, name="index"),
    path("contact", views.contact, name="contact")
]
