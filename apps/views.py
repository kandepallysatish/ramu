from django.shortcuts import render
from .models import Contactus
from django.contrib import messages
# Create your views here.


def index(request):
    return render(request, "index.html")

def contact(request):
    if request.method == "POST":
        name = request.POST.get('name')
        email = request.POST.get('email')
        number = request.POST.get('number')
        desc = request.POST.get('desc')
        cont = Contactus(name=name, email=email, number=number, desc=desc)
        cont.save()
        messages.success(request, "YOUR MESSAGE SEND SUCCESSFULLY.")
    return render(request, "contact.html")
