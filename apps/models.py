from django.db import models

# Create your models here.


class Contactus(models.Model):
    name = models.CharField(max_length=125)
    email = models.CharField(max_length=120)
    number = models.IntegerField(blank=True, unique=True)
    desc = models.TextField()

    def __str__(self):
        return self.name
